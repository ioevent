#!/bin/sh

LIBEV_SRC=../libev-3.2

cp $LIBEV_SRC/ev.h .
cp $LIBEV_SRC/event.h .
cp $LIBEV_SRC/ev_vars.h .
cp $LIBEV_SRC/ev_wrap.h .
cp $LIBEV_SRC/ev*.c .
