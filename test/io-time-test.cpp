/*
 * Embedded build:
 * c++ -I$(LIBEV_SRC) -o io-time-test -DEV_STANDALONE=1 io-time-test.cpp $(LIBEV_SRC)/ev.c $(LIBEV_SRC)/event.c
 *
 *
 * Wed 2007-12-19 - Modified by Chris Brody <chris.brody@gmail.com>
 *
 *     Adapted to test C++ inteface.
 *
 */


#include <sys/types.h>

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <sys/stat.h>
#ifndef WIN32
#include <sys/queue.h>
#include <unistd.h>
#endif
#include <time.h>
#ifdef HAVE_SYS_TIME_H
#include <sys/time.h>
#endif
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <ioevent>

int lasttime;

void
timeout_cb(int fd, short event, void * arg)
{
	itimer * timeout = (itimer *)arg;

	int newtime = time(NULL);

	printf("%s: called at %d: %d\n", __func__, newtime,
	    newtime - lasttime);

	lasttime = newtime;

	timeout->start(2);
}

int
main (int argc, char **argv)
{
	/* Initalize the io loop */
	IOLOOP l = ioloop_new();

	/* Init itimer */
	itimer timeout(timeout_cb, &timeout);
 
	timeout.start(2);

	lasttime = time(NULL);
	
	ioloop_dispatch(l);

	return (0);
}

