/*
 * Embedded build:
 * c++ -I$(LIBEV_SRC) -o io-test-eof -DEV_STANDALONE=1 io-test-eof.cpp $(LIBEV_SRC)/ev.c $(LIBEV_SRC)/event.c
 *
 *
 * Wed 2006-12-27 - Modified by Leandro Lucarella <llucax+eventxx@gmail.com>
 *
 *     Adapted to test the C++ inteface.
 *
 * Wed 2007-12-19 - Modified by Chris Brody <chris.brody@gmail.com>
 *
 *     Adapted to test the ioevent C++ inteface.
 *
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <unistd.h>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cerrno>

#include <ioevent>

int test_okay = 1;
int called = 0;

ioevent * ev;

void
read_cb(int fd, short event, void *arg)
{
	char buf[256];
	int len;

	len = read(fd, buf, sizeof(buf));

	printf("%s: read %d%s\n", __func__,
	    len, len ? "" : " - means EOF");

	if (len == 0) {
		ev->stop();

		if (called == 1)
			test_okay = 0;
	}

	called++;
}

int
main (int argc, char **argv)
{
	const char * test = "test string";
	int pair[2];

	IOLOOP loop = ioloop_new();

	if (socketpair(AF_UNIX, SOCK_STREAM, 0, pair) == -1)
		return (1);

	write(pair[0], test, strlen(test)+1);
	shutdown(pair[0], SHUT_WR);

	/* Init one event */
	ev = new ioevent(pair[1], EV_READ, read_cb, NULL);

	ev->start();

	ioloop_dispatch(loop);

	delete ev;

	return (test_okay);
}

