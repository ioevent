
#include <ioevent>

#include <EvBufferEvent.hpp>

#include <evhttp.h>

#include <iostream>

enum {
	STATE_INIT = 0,
	STATE_DATA
};

struct cc {
	int state;

	EvBufferEvent * be;
	struct bufferevent * bev;

	const char * cur_line;

};

void react_helo(struct cc * c)
{
	bufferevent * bev = c->bev;
	evbuffer * b = ::evbuffer_new();

	const char * line = c->cur_line;

	::evbuffer_add_printf(b, "250 Hello %s\r\n", line + 5);
	::bufferevent_write_buffer(bev, b);

	::evbuffer_free(b);
}

void react_250_ok(struct cc * c)
{
	bufferevent * bev = c->bev;
	evbuffer * b = ::evbuffer_new();

	::evbuffer_add_printf(b, "250 Ok\r\n");
	::bufferevent_write_buffer(bev, b);

	::evbuffer_free(b);
}

void react_data(struct cc * c)
{
	bufferevent * bev = c->bev;
	evbuffer * b = ::evbuffer_new();

	::evbuffer_add_printf(b, "354 End data with <CR><LF>.<CR><LF>\r\n");
	::bufferevent_write_buffer(bev, b);

	c->state = STATE_DATA;

	::evbuffer_free(b);
}

void react_data_line(struct cc * c)
{
	bufferevent * bev = c->bev;
	evbuffer * b = ::evbuffer_new();

	::evbuffer_add_printf(b, "250 Ok\r\n");
	::bufferevent_write_buffer(bev, b);

	c->state = STATE_INIT;

	::evbuffer_free(b);
}

void react_bye(struct cc * c)
{
	bufferevent * bev = c->bev;
	evbuffer * b = ::evbuffer_new();

	::evbuffer_add_printf(b, "221 Bye\r\n");
	::bufferevent_write_buffer(bev, b);

	::evbuffer_free(b);
}

void readcb(struct bufferevent *bev, void * v)
{
	char * line;

	struct cc * c = (struct cc *)v;

	while (line = ::evbuffer_readline(bev->input))
	{
		c->cur_line = line;

		if (c->state == STATE_INIT)
		{
			if (!strncmp(line, "HELO ", 5) ||
			    !strncmp(line, "EHLO ", 5))
			{
				react_helo(c);
			}
			else
			if (!strncmp(line, "MAIL", 4))
			{
				react_250_ok(c);
			}
			else
			if (!strncmp(line, "RCPT", 4))
			{
				react_250_ok(c);
			}
			else
			if (!strncmp(line, "DATA", 4))
			{
				react_data(c);
			}
			else
			if (!strncmp(line, "QUIT", 4))
			{
				react_bye(c);
			}
		}
		else
		{
			if (strcmp(line, "."))
			{
				react_data_line(c);
			}
		}

		free(line);
		c->cur_line = NULL;
	}

}

void writecb(struct bufferevent *, void *)
{

}

void errorcb(struct bufferevent *, short, void *)
{

}

void accept_handler(int s1, short, void *)
{
	struct cc * c = new cc;
	c->state = STATE_INIT;

	EvBufferEvent * be = new EvBufferEvent(readcb, writecb, errorcb, c);

	be->accept(s1);

	c->be = be;
	c->bev = be->_bufferevent;

	c->cur_line = NULL;

	evbuffer * b = ::evbuffer_new();

	::bufferevent_enable(be->_bufferevent, EV_READ);

	be->write("220 www.example.com ESMTP postfix\r\n", 35);

}

main()
{
	::event_init();

	int sock = ::bind_socket(NULL, 5525);

	::bufev_socket_listen(sock, 10);

	ioevent accept_event(sock, EV_READ, &accept_handler, NULL);
	::event_add(&accept_event, NULL);

	::event_dispatch();

}
